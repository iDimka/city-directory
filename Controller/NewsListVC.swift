//
//  NewsListVC.swift
//  Near By
//
//  Created by itechnotion on 28/06/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class NewsListVC: UIViewController {
@IBOutlet weak var tableNews: UITableView!
    var newsList = [NewsResult]()
    var newsCategory = String()
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableNews.rowHeight = UITableViewAutomaticDimension
        self.tableNews.estimatedRowHeight = 50
        self.tableNews.register(UINib(nibName: "CategorySearchCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.getNews()
    }

    func setIndicator(){
        indicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubview(toFront: view)
        indicator.hidesWhenStopped = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
    }
    func getNews(){
        self.setIndicator()
        let originalString = newsCategory
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        print(escapedString!)
        let parameter = "&filter[category]=".appending(escapedString!)
        Webservice.web_news.webserviceFetchItech(parameters: parameter){(parsedData,error,httpResponse) in
            DispatchQueue.main.async {
                if(error){
                    self.indicator.stopAnimating()
                    self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
                }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                    if parsedData["entries"] != nil {
                        let info = parsedData["entries"] as! [JSONType]
                        self.newsList = [NewsResult]()
                        let news = info.flatMap(NewsResult.init)
                        self.newsList.append(contentsOf: news)
                        self.tableNews.reloadData()
                        self.indicator.stopAnimating()
                    } else {
                    }
                }else if httpResponse.statusCode == 401{
                    var Error : String!
                    Error = parsedData["message"] as! String
                    self.ShowAlertMessage(title: "", message: Error, buttonText: "OK")
                }
            }
        }
    }
    // MARK: - user define methods
    
    func ShowAlertMessage(title : String, message: String, buttonText : String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
//MARK:- UITableview delegate and data source methods

extension NewsListVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.newsList.count == 0{
       }else{
        }
        return self.newsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategorySearchCell
        cell.stackRate.isHidden = true
        cell.imgProfile.image = self.newsList[indexPath.row].icon
        cell.lblName.text = self.newsList[indexPath.row].title!
        cell.lblAddress.text = self.newsList[indexPath.row].category!
        cell.lblRateCount.text = "By: ".appending(self.newsList[indexPath.row].author!)
        
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let newsDetail = self.storyboard?.instantiateViewController(withIdentifier: "NewsDetailsVC") as! NewsDetailsVC
        newsDetail.news = self.newsList[indexPath.row]
        self.navigationController?.pushViewController(newsDetail, animated: true)
        return indexPath
    }
}

//
//  LocalMapViewController.swift
//  Near By
//
//  Created by itechnotion on 22/05/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
import MapKit

class LocalMapViewController: UIViewController , MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!

    var favouriteDATA : [List]!
    var viewcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var locationData = CLLocationCoordinate2D(latitude: 0.0 ,longitude: 0.0)
    var data:[List] = [List]()
    var placeID = ""

    //MARK:- Life cycle methos
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
        self.fatchlocal_lat_long()
    }
    func fatchlocal_lat_long(){
        do {
            favouriteDATA = try viewcontext.fetch(List.fetchRequest()) as! [List]
            for index in favouriteDATA {
                data.append(index)
            }
        }
        catch {
            print(error.localizedDescription)
        }
        for var i in 0..<self.data.count  {
            self.locationData = CLLocationCoordinate2D(latitude: self.data[i].lat ,longitude: self.data[i].lat)
            self.mapView.showsUserLocation = true
            let annotation = MKPointAnnotation()
            annotation.coordinate = self.locationData
            annotation.title = self.data[i].name //"Demo"
            self.mapView.addAnnotation(annotation)
        }
        let span = MKCoordinateSpan(latitudeDelta: 0.5,longitudeDelta: 0.5)
        let region = MKCoordinateRegion(center: self.locationData, span: span)
        self.mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
        let name = view.annotation?.title as! String
        for i in 0..<self.data.count  {
            if name == data[i].name{
                self.placeID = data[i].placeid!
            }
        }
        viewDetail.isPlace = false
        viewDetail.placeID = self.placeID
        self.navigationController?.pushViewController(viewDetail, animated: true)
    }
}

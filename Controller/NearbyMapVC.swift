//
//  NearbyMapVC.swift
//  Near By
//
//  Created by itechnotion-mac1 on 06/04/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
import MapKit


class NearbyMapVC: UIViewController , MKMapViewDelegate {
    @IBOutlet weak var mapLocation: MKMapView!
    
    var placeid = ""
    var keyWords = String()
    var locationData = CLLocationCoordinate2D(latitude: 0.0 ,longitude: 0.0)
    var searchList = [SerachResultData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
        self.getMapLatitude()
    }
    
    func getMapLatitude(){
        let Hotels = "&location=".appending(String(appDelegate.latitude)).appending(",").appending(String(appDelegate.longitude))
            .appending("&radius=").appending(Constants.RADIUS)
            .appending("&keyword=").appending(keyWords)
            .appending("&sensor=").appending(Constants.SENSOR)
            .appending("&key=").appending(Constants.GOOGLEKEY)
        
        Webservice.web_map.webserviceFetchGetMethods(parameters: Hotels) { (parsedData, error, httpResponse) in
            self.searchList = [SerachResultData]()
            if(error){
                self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
            }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                if parsedData["results"] != nil {
                    let info = parsedData["results"] as! [JSONType]
                    let search = info.flatMap(SerachResultData.init)
                    self.searchList.append(contentsOf: search)
                    
                }
                for  i in 0..<self.searchList.count  {
                    self.locationData = CLLocationCoordinate2D(latitude: self.searchList[i].location.lat! ,longitude: self.searchList[i].location.lng!)
                    self.mapLocation.showsUserLocation = true
                    let span = MKCoordinateSpan(latitudeDelta: 0.01,longitudeDelta: 0.01)
                    let region = MKCoordinateRegion(center: self.locationData, span: span)
                    self.mapLocation.setRegion(region, animated: true)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = self.locationData
                    annotation.title = self.searchList[i].name //"Demo"
                    annotation.subtitle = self.searchList[i].vicinity
                    self.mapLocation.addAnnotation(annotation)
                    
//                    let initialLocation = CLLocation(latitude:self.searchList[i].location.lat!, longitude: self.searchList[i].location.lng!)
//
//
//                    let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate,
//                                                                              self.regionRadius, self.regionRadius)
//                    self.mapLocation.setRegion(coordinateRegion, animated: true)
                }
                
                self.locationData = CLLocationCoordinate2D(latitude: appDelegate.latitude ,longitude:appDelegate.longitude)
                self.mapLocation.showsUserLocation = true
                let span = MKCoordinateSpan(latitudeDelta: 0.7,longitudeDelta: 0.7)
                let region = MKCoordinateRegion(center: self.locationData, span: span)
                self.mapLocation.setRegion(region, animated: true)
            }
        }
    }
    // MARK: - Helper methods
    let regionRadius: CLLocationDistance = 5000
   
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation //as! Artwork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:
            MKLaunchOptionsDirectionsModeDriving]
        
//        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
        let name = view.annotation?.title as! String
        for i in 0..<self.searchList.count  {
            if name == searchList[i].name{
                self.placeid = searchList[i].place_id!
            }
        }
        viewDetail.placeID = self.placeid
        viewDetail.isPlace = false
        self.navigationController?.pushViewController(viewDetail, animated: true)
    }
    
    func ShowAlertMessage(title : String, message: String, buttonText : String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

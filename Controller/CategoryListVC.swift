//
//  CategoryListVC.swift
//  Near By
//
//  Created by itechnotion-mac1 on 26/03/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class CategoryListVC: UIViewController {
    @IBOutlet weak var tableCategory: UITableView!
    var categoryList = [CategorySection]()
    var category = [String]()
    // MARK:- Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.getCategoryList()
    }
    // MARK:- User define methods
    func getCategoryList(){
//        CategorySection
        var A = [Category]()
        A.append(Category(id:"A",title: "Accounting", value:"accounting"))
        A.append(Category(id:"A",title: "Airport", value:"airport"))
        A.append(Category(id:"A",title: "Amusement Park", value:"amusement_park"))
        A.append(Category(id:"A",title: "Aquarium", value:"aquarium"))
        A.append(Category(id:"A",title: "Art Gallery", value:"art_gallery"))
        A.append(Category(id:"A",title: "Atm", value:"atm"))
        var Cat = CategorySection(id: "A", title: A)
        categoryList.append(Cat)
        
        var B = [Category]()
        B.append(Category(id:"B",title: "Bakery", value:"bakery"))
        B.append(Category(id:"B",title: "Bank", value:"bank"))
        B.append(Category(id: "B",title: "Bar", value:"bar"))
        B.append(Category(id: "B",title: "Beauty Salon", value:"beauty_salon"))
        B.append(Category(id: "B",title: "Bicycle Store", value:"bicycle_store"))
        B.append(Category(id: "B",title: "Book Store", value:"book_store"))
        B.append(Category(id: "B",title: "Bowling Alley", value:"bowling_alley"))
        B.append(Category(id: "B",title: "Bus Station", value:"bus_station"))
        Cat = CategorySection(id: "B", title: B)
        categoryList.append(Cat)
        
        var C = [Category]()
        C.append(Category(id: "C",title: "Cafe", value:"cafe"))
        C.append(Category(id: "C",title: "Campground", value:"campground"))
        C.append(Category(id: "C",title: "Car Dealer", value:"car_dealer"))
        C.append(Category(id: "C",title: "Car Rental", value:"car_rental"))
        C.append(Category(id: "C",title: "Car Repair", value:"car_repair"))
        C.append(Category(id: "C",title: "Car Wash", value:"car_wash"))
        C.append(Category(id: "C",title: "Casino", value:"casino"))
        C.append(Category(id: "C",title: "Cemetery", value:"cemetery"))
        C.append(Category(id: "C",title: "Church", value:"church"))
        C.append(Category(id: "C",title: "City Hall", value:"city_hall"))
        C.append(Category(id: "C",title: "Clothing Store", value:"clothing_store"))
        C.append(Category(id: "C",title: "Convenience Store", value:"convenience_store"))
        C.append(Category(id: "C",title: "Courthouse", value:"courthouse"))
        Cat = CategorySection(id: "C", title: C)
        categoryList.append(Cat)
        
        var D = [Category]()
        D.append(Category(id: "D",title: "Dentist", value:"dentist"))
        D.append(Category(id: "D",title: "Department Store", value:"department_store"))
        D.append(Category(id: "D",title: "Doctor", value:"doctor"))
        Cat = CategorySection(id: "D", title: D)
        categoryList.append(Cat)
        
        var E = [Category]()
        E.append(Category(id: "E",title: "Electrician", value:"electrician"))
        E.append(Category(id: "E",title: "Electronics Store", value:"electronics_store"))
        E.append(Category(id: "E",title: "Embassy", value:"embassy"))
        Cat = CategorySection(id: "E", title: E)
        categoryList.append(Cat)
        
        var F = [Category]()
        F.append(Category(id: "F",title: "Fire Station", value:"fire_station"))
        F.append(Category(id: "F",title: "Florist", value:"florist"))
        F.append(Category(id: "F",title: "Funeral Home", value:"funeral_home"))
        F.append(Category(id: "F",title: "Furniture Store", value:"furniture_store"))
        Cat = CategorySection(id: "F", title: F)
        categoryList.append(Cat)
        
        var G = [Category]()
        G.append(Category(id: "G",title: "Gas Station", value:"gas_station"))
        G.append(Category(id: "G",title: "Gym", value:"gym"))
        Cat = CategorySection(id: "G", title: G)
        categoryList.append(Cat)
        
        var H = [Category]()
        H.append(Category(id: "H",title: "Hair Care", value:"hair_care"))
        H.append(Category(id: "H",title: "Hardware Store", value:"hardware_store"))
        H.append(Category(id: "H",title: "Hindu Temple", value:"hindu_temple"))
        H.append(Category(id: "H",title: "Home Goods Store", value:"home_goods_store"))
        H.append(Category(id: "H",title: "Hospital", value:"hospital"))
        Cat = CategorySection(id: "H", title: H)
        categoryList.append(Cat)
        
        var I = [Category]()
        I.append(Category(id: "I",title: "Insurance Agency", value:"insurance_agency"))
        Cat = CategorySection(id: "I", title: I)
        categoryList.append(Cat)
        
        var J = [Category]()
        J.append(Category(id: "J",title: "Jewelry Store", value:"jewelry_store"))
        Cat = CategorySection(id: "J", title: J)
        categoryList.append(Cat)
        
        var L = [Category]()
        L.append(Category(id: "L",title: "Laundry", value:"laundry"))
        L.append(Category(id: "L",title: "Lawyer", value:"lawyer"))
        L.append(Category(id: "L",title: "Library", value:"library"))
        L.append(Category(id: "L",title: "Liquor Store", value:"liquor_store"))
        L.append(Category(id: "L",title: "Local Government Office", value:"local_government_office"))
        L.append(Category(id: "L",title: "Locksmith", value:"locksmith"))
        L.append(Category(id: "L",title: "Lodging", value:"lodging"))
        Cat = CategorySection(id: "L", title: L)
        categoryList.append(Cat)
        
        var M = [Category]()
        M.append(Category(id: "M",title: "Meal Delivery", value:"meal_delivery"))
        M.append(Category(id: "M",title: "Meal Takeaway", value:"meal_takeaway"))
        M.append(Category(id: "M",title: "Mosque", value:"mosque"))
        M.append(Category(id: "M",title: "Movie Rental", value:"movie_rental"))
        M.append(Category(id: "M",title: "Movie Theatere", value:"movie_theater"))
        M.append(Category(id: "M",title: "Moving Company", value:"moving_company"))
        M.append(Category(id: "M",title: "museum", value:"museum"))
        Cat = CategorySection(id: "M", title: M)
        categoryList.append(Cat)
        
        var N = [Category]()
        N.append(Category(id: "N",title: "Night Club", value:"night_club"))
        Cat = CategorySection(id: "N", title: N)
        categoryList.append(Cat)
        
        var P = [Category]()
        P.append(Category(id: "P",title: "Painter", value:"painter"))
        P.append(Category(id: "P",title: "Park", value:"park"))
        P.append(Category(id: "P",title: "Parking", value:"parking"))
        P.append(Category(id: "P",title: "Pet Store", value:"pet_store"))
        P.append(Category(id: "P",title: "Pharmacy", value:"pharmacy"))
        P.append(Category(id: "P",title: "Physiotherapist", value:"physiotherapist"))
        P.append(Category(id: "P",title: "Plumber", value:"plumber"))
        P.append(Category(id: "P",title: "Police", value:"police"))
        P.append(Category(id: "P",title: "Post office", value:"post_office"))
        Cat = CategorySection(id: "P", title: P)
        categoryList.append(Cat)
        
        var R = [Category]()
        R.append(Category(id: "R",title: "Real Estate Agency", value:"real_estate_agency"))
        R.append(Category(id: "R",title: "Restaurant", value:"restaurant"))
        R.append(Category(id: "R",title: "Roofing Contractor", value:"roofing_contractor"))
        R.append(Category(id: "R",title: "Rv Park", value:"rv_park"))
        Cat = CategorySection(id: "R", title: R)
        categoryList.append(Cat)
        
        var S = [Category]()
        S.append(Category(id: "S",title: "School", value:"school"))
        S.append(Category(id: "S",title: "Shoe Store", value:"shoe_store"))
        S.append(Category(id: "S",title: "Shopping mall", value:"shopping_mall"))
        S.append(Category(id: "S",title: "spa", value:"spa"))
        S.append(Category(id: "S",title: "Stadium", value:"stadium"))
        S.append(Category(id: "S",title: "Storage", value:"storage"))
        S.append(Category(id: "S",title: "Store", value:"store"))
        S.append(Category(id: "S",title: "Subway Station", value:"subway_station"))
        S.append(Category(id: "S",title: "Synagogue", value:"synagogue"))
        Cat = CategorySection(id: "S", title: S)
        categoryList.append(Cat)
        
        var T = [Category]()
        T.append(Category(id: "T",title: "Taxi Stand", value:"taxi_stand"))
        T.append(Category(id: "T",title: "Train Station", value:"train_station"))
        T.append(Category(id: "T",title: "Transit Station", value:"transit_station"))
        T.append(Category(id: "T",title: "Travel Agency", value:"travel_agency"))
        Cat = CategorySection(id: "T", title: T)
        categoryList.append(Cat)
        
        var U = [Category]()
        U.append(Category(id: "U",title: "University", value:"university"))
        Cat = CategorySection(id: "U", title: U)
        categoryList.append(Cat)
        
        var V = [Category]()
        V.append(Category(id: "V",title: "Veterinary Care", value:"veterinary_care"))
        Cat = CategorySection(id: "V", title: V)
        categoryList.append(Cat)
        
        var Z = [Category]()
        Z.append(Category(id: "Z",title: "Zoo", value:"zoo"))
        Cat = CategorySection(id: "Z", title: Z)
        categoryList.append(Cat)
        for i in 0..<categoryList.count{
            category.append(categoryList[i].id)
        }
        self.tableCategory.reloadData()
    }
    @IBAction func MapAction(_ sender: UIBarButtonItem) {
        let viewMap = self.storyboard?.instantiateViewController(withIdentifier: "NearbyMapVC") as! NearbyMapVC
        viewMap.keyWords = "hotel"
        self.navigationController?.pushViewController(viewMap, animated: true)
    }
}
extension CategoryListVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
        return categoryList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return categoryList.count
        return categoryList[section].title.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        let cat = categoryList[indexPath.section].title[indexPath.row]
        let label1 = cell.viewWithTag(1) as! UILabel // 1 is tag of first label;
        label1.text = cat.title
        return cell
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let viewControllSearch = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoryVC") as! SearchCategoryVC
        viewControllSearch.searchKey = categoryList[indexPath.section].title[indexPath.row].value!
        viewControllSearch.searchKeyData = categoryList[indexPath.section].title[indexPath.row].title!
        self.navigationController?.pushViewController(viewControllSearch, animated: true)
        return indexPath
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return categoryList[section].id
    }
    
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return category
    }
}


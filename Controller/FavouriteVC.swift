//
//  FavouriteVC.swift
//  Near By
//
//  Created by itechnotion on 08/05/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
import CoreData

class FavouriteVC: UIViewController {
    
    @IBOutlet weak var lblmyfavourite_place: UILabel!
    @IBOutlet weak var lblNooData: UILabel!
    @IBOutlet weak var tableviewOutlet: UITableView!
   
    var favouriteDATA : [List]!
    var viewcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var data:[List] = [List]()
    var placeID = ""
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
        self.tableviewOutlet.rowHeight = UITableViewAutomaticDimension
        self.tableviewOutlet.estimatedRowHeight = 50
        self.tableviewOutlet.register(UINib(nibName: "CategorySearchCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.fetchlocalfavouritedata()
        if data.count == 0 && data == [] {
            tableviewOutlet.isHidden = true
             self.lblmyfavourite_place.isHidden = true
        } else {
            tableviewOutlet.isHidden = false
            self.lblNooData.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.favouriteDATA.removeAll()
        self.data.removeAll()
        self.fetchlocalfavouritedata()
    }
    //MARK:- IBAction methods (Clieck events)
    @IBAction func MapButton(_ sender: UIBarButtonItem) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "localmap") as! LocalMapViewController
        next.placeID = self.placeID
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    //MARK:- User define methods
    func fetchlocalfavouritedata() {
        do {
            favouriteDATA = try viewcontext.fetch(List.fetchRequest()) as! [List]
            for index in favouriteDATA {
                data.append(index)
            }
           self.tableviewOutlet.reloadData()
          }
        catch {
          print(error.localizedDescription)
        }
    }
}

//MARK:- UITableview Delegate and data source methods

extension FavouriteVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategorySearchCell
        if data[indexPath.row].image != nil {
            cell.imgProfile.image = UIImage(data: self.data[indexPath.row].image! as Data)
        }  else {
            TableViewHelper.EmptyMessage(message: "No Data Available", viewController: self.tableviewOutlet)
        }
        cell.lblName.text = self.data[indexPath.row].name
        cell.lblAddress.text = self.data[indexPath.row].address
        self.placeID = self.data[indexPath.row].placeid!
        if self.data[indexPath.row].rating != nil{
            cell.lblRateCount.text = self.data[indexPath.row].rating!
            cell.stackRate.ratingCount = Double(self.data[indexPath.row].rating!)!
            cell.stackRate.ratingButton(button: Double(self.data[indexPath.row].rating!)!)
        }else{
            cell.lblRateCount.text = "0"
            cell.stackRate.ratingCount = 0
            cell.stackRate.ratingButton(button: 0)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
        viewDetail.placeID = data[indexPath.row].placeid!
        viewDetail.isPlace = false
        self.navigationController?.pushViewController(viewDetail, animated: true)
        
    }
}

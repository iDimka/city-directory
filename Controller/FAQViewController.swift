//
//  FAQViewController.swift
//  Near By
//
//  Created by itechnotion on 10/05/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController , ExpandableHeaderViewDelegate {
    @IBOutlet weak var tableviewOutlet: UITableView!
    
    var listData = [FAQList(title: "Is City Directory is like a telephone directory ?",
                            description: ["No it is not a telephone directory of City.It is a online directory,which gives the information about all the useful near by place."],
                            expanded: false) ,
        FAQList(title: "How long it takes for places to be added to Maps",
                description: ["Google review your edits, so your changes might take some time to be updated on the map. Google may email you about the status of your edits and may forward you questions from other people who review your edits."],
                expanded: false) ,
        FAQList(title: "How can add information about business?",
                description: ["To update your business information on Google Maps:\nIf you own a business, use Google My Business to manage your listing’s information for free. You'll be able to quickly manage details like your business hours, address, and photos.\n To update your business information on Google Maps:Goto Pages \n1)Claim and verify your business using your computer \n And 2)        Claim and verify your business using your phone or tablet"],
                expanded: false)]

    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
    }
    func toggleSection(header: ExpandableHeaderView, section: Int) {
        listData[section].expanded = !listData[section].expanded
        tableviewOutlet.beginUpdates()
        tableviewOutlet.reloadData()
        for i in 0...2 {
            if section != i {
                listData[i].expanded = false
            }
        }
        tableviewOutlet.endUpdates()
    }
}
//MARK:- UITableview delegate and data source methods
extension FAQViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return listData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData[section].description.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return listData[section].title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if (listData[indexPath.section].expanded) {
            return UITableViewAutomaticDimension
        }else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (listData[indexPath.section].expanded) {
            return UITableViewAutomaticDimension
        }else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        header.customInit(title: listData[section].title, section: section, delegate: self as ExpandableHeaderViewDelegate)
        header.textLabel?.font = UIFont(name: "Apple SD Gothic Neo", size: 16)
        header.textLabel?.textColor = UIColor(red: 246/255, green: 129/255, blue: 89/255, alpha: 1.0)
        
        var image = UIImageView(frame: CGRect(x: self.tableviewOutlet.frame.size.width - 40, y: 25 - 10 , width: 20 , height: 20))
        image.image = #imageLiteral(resourceName: "down-arrow")
        if listData[section].expanded == true {
            UIView.animate(withDuration: 0.0, animations: {
                image.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
            })
        }
        header.addSubview(image)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textColor = UIColor.gray
        cell.separatorInset = .zero
        cell.textLabel?.font = UIFont(name: "Apple SD Gothic Neo", size: 15)
        cell.textLabel?.textAlignment = NSTextAlignment.justified
        cell.textLabel?.text = listData[indexPath.section].description[indexPath.row]
        return cell
    }
}



//
//  ViewController.swift
//  Near By
//
//  Created by itechnotion on 18/05/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class ImageViewerController: UIViewController {

//    @IBOutlet weak var photo_Img: UIImageView!
    @IBOutlet weak var scroll: UIScrollView!
    
    var photo = [UIImage]()
    var pages = Int()
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    override func viewDidLoad() {
        super.viewDidLoad()
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
            navigationItem.backBarButtonItem = backItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        indicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubview(toFront: view)
        indicator.hidesWhenStopped = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(scrolling), userInfo: nil, repeats: false)
    }

    @objc func scrolling(){
        for i in 0..<photo.count{
            let image = UIImageView(frame: CGRect(x: (self.view.frame.size.width*CGFloat(i)), y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
            image.image = photo[i]
            image.contentMode = .scaleAspectFit
            scroll.addSubview(image)
        }
        scroll.contentSize = CGSize(width:(self.view.frame.width * CGFloat(photo.count)), height: CGFloat(self.view.frame.size.height))
        self.scrollToPage(page: pages, animated: false)
    }
   
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.view.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scroll.scrollRectToVisible(frame, animated: animated)
        indicator.stopAnimating()
    }
}

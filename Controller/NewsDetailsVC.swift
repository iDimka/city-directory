//
//  NewsDetailsVC.swift
//  Near By
//
//  Created by itechnotion on 28/06/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
class NewsDetailsVC: UIViewController {
    @IBOutlet weak var viewDescriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var viewDescription: UIView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    var news = NewsResult()
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBanner.image = news.icon
        lblTitle.text = news.title
        lblCategory.text = news.category
        lblDate.text = news.date?.appending(", ").appending(news.time!)
        lblDescription.text = news.description
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setHeight), userInfo: nil, repeats: false)
    }
    @objc func setHeight(){
        let h = lblDescription.frame.size.height + lblDescription.frame.origin.y + 10
        viewDescriptionHeightConstraint.constant = h
        let heights = h+viewDescription.frame.origin.y
        scroll.contentSize = CGSize(width:self.view.frame.width, height: CGFloat(heights))
    }
}

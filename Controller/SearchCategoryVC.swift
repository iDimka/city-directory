//
//  SearchCategoryVC.swift
//  Near By
//
//  Created by itechnotion-mac1 on 26/03/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class SearchCategoryVC: UIViewController {
    @IBOutlet weak var tableSearch: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchImage: UIImageView!
    var nextPageToken = String()
    var isLoad = true
    var searchKey = String()
    var searchKeyData = String()
    var searchList = [SerachResultData]()
    var placeList = [Place]()
    
//    var workItem = DispatchWorkItem{}
    var serviceCalled = false
      let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tableSearch.register(UINib(nibName: "NewsHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "NewsHeader")
        self.searchImage.tintColor = #colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem

        txtSearch.text = searchKeyData
        self.tableSearch.rowHeight = UITableViewAutomaticDimension
        self.tableSearch.estimatedRowHeight = 50
        self.tableSearch.register(UINib(nibName: "CategorySearchCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.getPlaceCategory()
        self.getCategoryList(isLoard: false)
    }
    
    //MARK:- IBAction methods (Clieck events)f
    @IBAction func actionSearchText(_ sender: Any) {
    
    }
    @IBAction func actionMapView(_ sender: Any) {
        let viewMap = self.storyboard?.instantiateViewController(withIdentifier: "NearbyMapVC") as! NearbyMapVC
        viewMap.keyWords = self.searchKey
        self.navigationController?.pushViewController(viewMap, animated: true)
    }
    
    @IBAction func searchButton(_ sender: UIButton) {
        self.searchKey = self.txtSearch.text!
        self.searchKeyData = self.txtSearch.text!
        self.getPlaceCategory()
        self.getCategoryList(isLoard: false)
    }
    //MARK:- User define methods (get place category)
    
    func getPlaceCategory(){
        let originalString = searchKeyData
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        print(escapedString!)
        let parameter = "&filter[category]=".appending(escapedString!)
        Webservice.web_place.webserviceFetchItech(parameters: parameter){(parsedData,error,httpResponse) in
            DispatchQueue.main.async {
                if(error){
                    self.indicator.stopAnimating()
                    self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
                }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                    self.placeList = [Place]()
                    if parsedData["entries"] != nil {
                        let info = parsedData["entries"] as! [JSONType]
                        let place = info.flatMap(Place.init)
                        self.placeList.append(contentsOf: place)
                        self.tableSearch.reloadData()
                    } else {
                    }
                }else if httpResponse.statusCode == 401{
                    var Error : String!
                    Error = parsedData["message"] as! String
                    self.ShowAlertMessage(title: "", message: Error, buttonText: "OK")
                }
            }
        }
    }
    
    //MARK:- Get category list
    func getCategoryList(isLoard: Bool){
        if searchKeyData == ""{
            searchKeyData = "hotel"
        }
        var param = ""
        if isLoard {
            param = "key=".appending(Constants.GOOGLEKEY)
                .appending("&pagetoken=").appending(nextPageToken)
        }else{
            self.searchList = [SerachResultData]()
            indicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
            indicator.center = view.center
            view.addSubview(indicator)
            indicator.bringSubview(toFront: view)
            indicator.hidesWhenStopped = true
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            indicator.startAnimating()
            let originalString = searchKey
            let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            param = "&location=".appending(String(appDelegate.latitude)).appending(",").appending(String(appDelegate.longitude))
                .appending("&radius=").appending(Constants.RADIUS)
                .appending("&keyword=").appending(searchKeyData)
                .appending("&sensor=").appending(Constants.SENSOR)
                .appending("&key=").appending(Constants.GOOGLEKEY)
            print(param)
        }
        
        
//        let workItem = DispatchWorkItem{
        Webservice.web_map.webserviceFetchGetMethods(parameters: param) { (parsedData, error, httpResponse) in
            
            if(error){
                self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
            }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                
                if parsedData["results"] != nil{
                    let info = parsedData["results"] as! [JSONType]
                    let search = info.flatMap(SerachResultData.init)
                    self.searchList.append(contentsOf: search)
                    print(self.searchList)
                    self.serviceCalled = true
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.indicator.stopAnimating()
                    self.tableSearch.reloadData()
                }
                if parsedData["next_page_token"] != nil{
                    self.nextPageToken = parsedData["next_page_token"] as! String
                    self.isLoad = true
                }else{
                    self.isLoad = false
                }
            }
        }
//        }
//        workItem.cancel()
    }
    
    func ShowAlertMessage(title : String, message: String, buttonText : String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

}

//MARK:- UITableview delegate and datasource methods

extension SearchCategoryVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.placeList.count
        }else{
        if self.searchList.count == 0{
            if serviceCalled{
                TableViewHelper.EmptyMessage(message: "No Data Available", viewController: self.tableSearch)
            }else{
                //TableViewHelper.loadGIF(viewController: self.tableSearch)
            }
        }else{
            TableViewHelper.EmptyMessage(message: "", viewController: self.tableSearch)
        }
         return self.searchList.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CategorySearchCell
        if indexPath.section == 0{
            cell.lblName.text = self.placeList[indexPath.row].name
            let newAddress = (self.placeList[indexPath.row].address!).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
            cell.lblAddress.text = newAddress//self.placeList[indexPath.row].address
            if self.placeList[indexPath.row].rating != nil{
                cell.lblRateCount.text = String(describing: self.placeList[indexPath.row].rating!)
                cell.stackRate.ratingCount = Double(self.placeList[indexPath.row].rating!)!
                cell.stackRate.ratingButton(button: Double(self.placeList[indexPath.row].rating!)!)
            }else{
                cell.lblRateCount.text = "0"
                cell.stackRate.ratingCount = 0
                cell.stackRate.ratingButton(button: 0)
            }
            if placeList[indexPath.row].photo.count > 0{
                let urlString = (itechBase).appending(placeList[indexPath.row].photo[0].path!)
                let url = URL(string: urlString)
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    cell.imgProfile.image = UIImage(data: imageData)
                }
            }
        }else{
            cell.lblName.text = self.searchList[indexPath.row].name
            cell.lblAddress.text = self.searchList[indexPath.row].vicinity
            if self.searchList[indexPath.row].rating != nil{
                cell.lblRateCount.text = String(describing: self.searchList[indexPath.row].rating!)
                cell.stackRate.ratingCount = Double(self.searchList[indexPath.row].rating!)
                cell.stackRate.ratingButton(button: Double(self.searchList[indexPath.row].rating!))
            }else{
                cell.lblRateCount.text = "0"
                cell.stackRate.ratingCount = 0
                cell.stackRate.ratingButton(button: 0)
            }
            if searchList[indexPath.row].photos.count > 0{
                if self.searchList[indexPath.row].photos[0].images == nil{
                    let photo_reference =  self.searchList[indexPath.row].photos[0].photo_reference
                    let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=170").appending("&photoreference=").appending(photo_reference!).appending("&key=").appending(Constants.GOOGLEKEY)
                    let url = URL(string: urlString)
                    let data = try? Data(contentsOf: url!)
                    if let imageData = data {
                        cell.imgProfile.image = UIImage(data: imageData)
                        self.searchList[indexPath.row].photos[0].images = cell.imgProfile.image
                    }
                }
//                cell.imgProfile.image = searchList[indexPath.row].photos[0].images
            }
            if indexPath.row == self.searchList.count - 1{
                if isLoad{
                    
                    self.getCategoryList(isLoard: true)
                 }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 0{
            let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
            viewDetail.isPlace = true
            viewDetail.places = placeList[indexPath.row]
            self.navigationController?.pushViewController(viewDetail, animated: true)
        }else{
            let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
            viewDetail.placeID = searchList[indexPath.row].place_id!
            viewDetail.isPlace = false
            self.navigationController?.pushViewController(viewDetail, animated: true)
        }
        return indexPath
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var title = "Places by Google" // Set Title text for section header
        if section == 0{
            title = "Feature places"
        }
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NewsHeader") as! NewsHeader
        headerView.lblTitle.text = title
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if self.placeList.count > 0{
                return 30
            }else{
                return 0
            }
        }else{
            return 30.0
        }
    }
}

//MARK:- UITextfield delegate and datasource methods

extension SearchCategoryVC  : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if !(textField.text?.isEmpty)!{
            searchKey = textField.text!
            self.getPlaceCategory()
            self.getCategoryList(isLoard: false)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            if txtAfterUpdate == " "{
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }
}

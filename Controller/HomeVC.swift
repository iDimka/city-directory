//  HomeVC.swift
//  Near By
import UIKit
import SDWebImage
import GoogleMobileAds
import CoreLocation
import CoreData

var NETWORKERROR = "Please Check Your Network"



class HomeVC: UIViewController , CLLocationManagerDelegate {
    
    var bannerView: GADBannerView!

    var locationManager = CLLocationManager()
    var favouriteDATA : [List]!
   // var localSlider1data : [Sliderlist1]!
   // var localSlider2data : [Sliderlist2]!
    var viewcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var data = [String]()
   
    let user = UserDefaults.standard

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var collectionCategory: UICollectionView!
    @IBOutlet weak var collectionCategorySlider1: UICollectionView!
    @IBOutlet weak var collectionCategorySlider2: UICollectionView!
    @IBOutlet weak var collectionNews: UICollectionView!
    
    @IBOutlet weak var viewAddBanner: UIView!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var viewCategory: UIView!
    @IBOutlet var viewNews: UIView!
    @IBOutlet var viewFirstSlider: UIView!
    @IBOutlet var viewSecondSlider: UIView!
    @IBOutlet weak var searchImage: UIImageView!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    var sliderList1 = [CategoryData]()
    var sliderList2 = [CategoryData]()
    var categoryList = [CategoryList]()
    var newsList = [String]()
    var latitude = 0.0
    var longitude = 0.0

    var flag = 0
    var distanceInMeters = Double()
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.data.removeAll()
        self.latitude = 23.033863
        self.longitude = 72.585022
        self.callDelayMethods()
        self.searchImage.tintColor = #colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes

        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
        
        self.txtSearch.clearsOnBeginEditing = true
        let nibName = UINib(nibName: "homeNearbyCell", bundle:nil)
        collectionCategory.register(nibName, forCellWithReuseIdentifier: "cell")
        let nibNews = UINib(nibName: "NewsCell", bundle:nil)
        collectionNews.register(nibNews, forCellWithReuseIdentifier: "cell")
        let nibNameSlider1 = UINib(nibName: "CategorySliderCell", bundle:nil)
        collectionCategorySlider1.register(nibNameSlider1, forCellWithReuseIdentifier: "cell")
        collectionCategorySlider2.register(nibNameSlider1, forCellWithReuseIdentifier: "cell")
        
        self.getCategoryList()
        self.collectionCategory.reloadData()
          NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.fetchlocalfavouritedata()
        viewSearch.frame = (CGRect(x: 0, y: 0, width: self.view.frame.width, height: 98))
        self.scroll.addSubview(viewSearch)
        viewCategory.frame = (CGRect(x: 0, y: 98, width: self.view.frame.width, height: ((((self.view.frame.size.width - 25)/4)+25)*3)))
        self.scroll.addSubview(viewCategory)
        viewNews.frame = (CGRect(x: 0, y: viewCategory.frame.size.height+viewCategory.frame.origin.y, width: self.view.frame.width, height: 65))
        self.scroll.addSubview(viewNews)
        viewFirstSlider.frame = (CGRect(x: 0, y: viewNews.frame.size.height+viewNews.frame.origin.y, width: self.view.frame.width, height: 205))
        self.scroll.addSubview(viewFirstSlider)
        viewSecondSlider.frame = (CGRect(x: 0, y: viewFirstSlider.frame.size.height+viewFirstSlider.frame.origin.y+20, width:
            self.view.frame.width, height: 205))
        self.scroll.addSubview(viewSecondSlider)
        scroll.contentSize = CGSize(width:self.view.frame.width, height: viewSecondSlider.frame.size.height + viewSecondSlider.frame.origin.y + 50)
        UIView.animate(withDuration: 0, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
        UIView.animate(withDuration: 0, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
            self.setAd()
    }
    
    
    // MARK: - Locationmanager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0]
        self.latitude = userLocation.coordinate.latitude
        self.longitude = userLocation.coordinate.longitude
        if flag == 0{
            self.callDelayMethods()
        }
    }
    
    // MARK: - Method for delay call    (Call after fatching lat lang )
    func callDelayMethods(){
        //         self.latitude = 23.033863
        //        self.longitude = 72.585022
        //        let coordinate₀ = CLLocation(latitude: latitude, longitude: longitude)
        //        let coordinate₁ = CLLocation(latitude: 5.0, longitude: 3.0)
        //        self.distanceInMeters = coordinate₀.distance(from: coordinate₁)
        flag = 1
        self.getNews()
        //        DispatchQueue.global(qos: .background).async {
        self.getCategorySlider1()
        //        }
        //        DispatchQueue.global(qos: .background).async {
        self.getCategorySlider2()
        //        }
        //        dispatch_async(DispatchQueue.global(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
        //            self.getCategorySlider1()
        //        }
        //        dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0).async() {
        //            self.getCategorySlider2()
        //        }
        self.fetchlocalfavouritedata()
    }
    
    // MARK: - IBAtion Methods (Click events)
    @IBAction func SearchButton(_ sender: UIButton) {
        if (!(txtSearch.text?.isEmpty)!){
            let viewControllSearch = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoryVC") as! SearchCategoryVC
            viewControllSearch.searchKey = txtSearch.text!
            viewControllSearch.searchKeyData = txtSearch.text!
            self.navigationController?.pushViewController(viewControllSearch, animated: true)
        }
    }

    @IBAction func actionMapView(_ sender: Any) {
       let viewMap = self.storyboard?.instantiateViewController(withIdentifier: "NearbyMapVC") as! NearbyMapVC
        viewMap.keyWords = "hotel"
        self.navigationController?.pushViewController(viewMap, animated: true)
    }
    
    // MARK: - User define methods
    func fetchlocalfavouritedata() {
        do {
            favouriteDATA = try viewcontext.fetch(List.fetchRequest()) as! [List]
            for index in favouriteDATA {
                data.append(index.placeid!)
            }
            self.collectionCategorySlider1.reloadData()
            self.collectionCategorySlider2.reloadData()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    @objc func rotated() {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            print("Landscape")
            UIView.animate(withDuration: 0, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
            print("Portrait")
            UIView.animate(withDuration: 0, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func getCategoryList(){
        var category = CategoryList(imgProfile: #imageLiteral(resourceName: "img_bank"), title: "Bank", id: "bank")
        categoryList.append(category)
        
        category = CategoryList(imgProfile: #imageLiteral(resourceName: "img_hotal"), title: "Hotel", id: "hotel")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_restorant"), title: "Restaurant", id: "restaurant")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "icon_library"), title: "Library", id: "library")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_night_club"), title: "Night Club", id: "Night Club")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_clothing"), title: "Clothing", id: "Clothing")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_shopping_mall"), title: "Shopping Mall", id: "Shopping Mall")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "icon_salon"), title: "Salon", id: "salon")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_parking"), title: "Parking", id: "parking")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_theater"), title: "Theater", id: "theater")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "icon_petrolpump"), title: "Petrol Pump", id: "petrol")
        categoryList.append(category)
        
        category = CategoryList(imgProfile:#imageLiteral(resourceName: "img_more"), title: "More", id: "Demo")
        categoryList.append(category)
    }
    
    func getNews(){
        Webservice.web_news.webserviceFetchItech(parameters: ""){(parsedData,error,httpResponse) in
            DispatchQueue.main.async {
                if(error){
                    self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
                }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                    if parsedData["entries"] != nil {
                        let info = parsedData["fields"] as! JSONType
                        let categoryNews = info["category"] as! JSONType
                        let option = categoryNews["options"] as! JSONType
                        let op = option["options"] as? String
                        self.newsList  = op!.components(separatedBy: ",")
                        self.collectionNews.reloadData()
                    } else {
                    }
                }else if httpResponse.statusCode == 401{
                    var Error : String!
                    Error = parsedData["message"] as! String
                    self.ShowAlertMessage(title: "", message: Error, buttonText: "OK")
                }
            }
        }
    }
    
    func getCategorySlider1 (){
        self.data.removeAll()
//        DispatchQueue.global(qos: .background).async {
        self.getCategoryData(type: "Hotel") { (category, status, message) in
            if status{
                self.sliderList1 = category
                self.collectionCategorySlider1.reloadData()
            }else{
                self.ShowAlertMessage(title: "", message: message, buttonText: "OK")
            }
        }
//        }
    }

    func getCategorySlider2(){
        self.data.removeAll()
//        DispatchQueue.global(qos: .background).async {
        self.getCategoryData(type: "restaurant") { (category, status, message) in
            if status{
                self.sliderList2 = category
                self.collectionCategorySlider2.reloadData()
            }else{
                self.ShowAlertMessage(title: "", message: message, buttonText: "OK")
            }
        }
//        }
    }
    
    func convertToDictionary(text: String) -> JSONType? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    // MARK: - user define methods
    func ShowAlertMessage(title : String, message: String, buttonText : String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - UICollectionView Delegate And Data source methods

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    //1
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //2
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionCategory{
            return categoryList.count
        }else if collectionView ==  collectionCategorySlider1{
            return self.sliderList1.count
        }else if collectionView ==  collectionCategorySlider2{
            return self.sliderList2.count
        }else{
             return  self.newsList.count
        }
    }
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionCategory{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                          for: indexPath) as! homeNearbyCell
            cell.imgCategory.image = categoryList[indexPath.row].imgProfile
            cell.lblTitle.text =  categoryList[indexPath.row].title
            return cell
        }else  if collectionView ==  collectionCategorySlider1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                          for: indexPath) as! CategorySliderCell
            
            DispatchQueue.main.async {
                //        print(self.width)
                if self.sliderList1[indexPath.row].photos.count > 0{
                    if self.sliderList1[indexPath.row].photos[0].images == nil{
                        let photo_reference =  self.sliderList1[indexPath.row].photos[0].photo_reference
                        
                        let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=170").appending("&photoreference=").appending(photo_reference!).appending("&key=").appending(Constants.GOOGLEKEY)
                        let url = URL(string: urlString)
                        let data = try? Data(contentsOf: url!)
                        
                        
                        if let imageData = data {
                            //                cell.imgProfile.boderRoundwithShadow()
                            cell.imgIcon.image = UIImage(data: imageData)
                            self.sliderList1[indexPath.row].photos[0].images = cell.imgIcon.image
                        }
                    }else{
                                cell.imgIcon.image = self.sliderList1[indexPath.row].photos[0].images
                    }
                }
                
                //                if self.sliderList1[indexPath.row].photos.count > 0{
                //                    cell.imgIcon.image = self.sliderList1[indexPath.row].photos[0].images
                //                }
                
                cell.lblName.text = self.sliderList1[indexPath.row].name!
                print(self.sliderList1[indexPath.row].name!)
                if self.sliderList1[indexPath.row].vicinity != nil {
                    cell.lblAddress.text = self.sliderList1[indexPath.row].vicinity!
                }
                for index in 0..<self.data.count {
                    if self.data[index] == self.sliderList1[indexPath.row].place_id {
                        cell.favouriteButtonOutlet.setImage(#imageLiteral(resourceName: "fill_heart"), for: .normal)
                    }
                }
            }
            return cell
        }else   if collectionView ==  collectionCategorySlider2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                          for: indexPath) as! CategorySliderCell
            DispatchQueue.main.async {
                if self.sliderList2[indexPath.row].photos.count > 0{
                    if self.sliderList2[indexPath.row].photos[0].images == nil{
                        let photo_reference =  self.sliderList2[indexPath.row].photos[0].photo_reference
                        let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=170").appending("&photoreference=").appending(photo_reference!).appending("&key=").appending(Constants.GOOGLEKEY)
                        let url = URL(string: urlString)
                        let data = try? Data(contentsOf: url!)
                        if let imageData = data {
                            cell.imgIcon.image = UIImage(data: imageData)
                            self.sliderList2[indexPath.row].photos[0].images = cell.imgIcon.image
                        }
                    }else{
                        cell.imgIcon.image = self.sliderList2[indexPath.row].photos[0].images
                        
                    }
                    cell.lblName.text = self.sliderList2[indexPath.row].name!
                    cell.lblAddress.text = self.sliderList2[indexPath.row].vicinity!
                    for index in 0..<self.data.count {
                        if self.data[index] == self.sliderList2[indexPath.row].place_id! {
                            cell.favouriteButtonOutlet.setImage(#imageLiteral(resourceName: "fill_heart"), for: .normal)
                        }
                    }
                }
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                          for: indexPath) as! NewsCell
            cell.lblNews.text = self.newsList[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionCategory{
            if indexPath.row == 11{
                let categoryView = self.storyboard?.instantiateViewController(withIdentifier: "CategoryListVC") as! CategoryListVC
                self.navigationController?.pushViewController(categoryView, animated: true)
            }else{
                let viewControllSearch = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoryVC") as! SearchCategoryVC
                viewControllSearch.searchKey = categoryList[indexPath.row].id!
                viewControllSearch.searchKeyData = categoryList[indexPath.row].title!
                self.navigationController?.pushViewController(viewControllSearch, animated: true)
            }
        }else if collectionView == collectionCategorySlider1{
            let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
            viewDetail.isPlace = false
            viewDetail.placeID = sliderList1[indexPath.row].place_id!
            self.navigationController?.pushViewController(viewDetail, animated: true)
        }else if collectionView == collectionCategorySlider2{
            let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "CategoryDetailsVC") as! CategoryDetailsVC
            viewDetail.isPlace = false
            viewDetail.placeID = sliderList2[indexPath.row].place_id!
            self.navigationController?.pushViewController(viewDetail, animated: true)
        }else{
            let viewDetail = self.storyboard?.instantiateViewController(withIdentifier: "NewsListVC") as! NewsListVC
            viewDetail.newsCategory = self.newsList[indexPath.row]
            self.navigationController?.pushViewController(viewDetail, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionCategory{
            return CGSize(width: (self.view.frame.size.width - 20)/4, height: (((self.view.frame.size.width - 20)/4)+20))
        }else if collectionView == collectionNews{
            return CGSize(width: (138), height: (50))
        }else{
            return CGSize(width: (138), height: (152))
        }
    }
}

// MARK: - UITextfield Delegate And Data source methods

extension HomeVC  : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (!(textField.text?.isEmpty)!){
            let viewControllSearch = self.storyboard?.instantiateViewController(withIdentifier: "SearchCategoryVC") as! SearchCategoryVC
            viewControllSearch.searchKey = textField.text!
            viewControllSearch.searchKeyData = textField.text!
            self.navigationController?.pushViewController(viewControllSearch, animated: true)
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            if txtAfterUpdate == " "{
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }
}

// MARK: - Get category data extension
extension HomeVC{
    func getCategoryData(type: String, completion:(([CategoryData],(Bool),(String))->())?) {
        let Hotels = "&location=".appending(String(latitude)).appending(",").appending(String(longitude))
            .appending("&radius=").appending(Constants.RADIUS)
            .appending("&keyword=").appending(type)//keyword//type
            .appending("&sensor=").appending(Constants.SENSOR)
            .appending("&key=").appending(Constants.GOOGLEKEY)
        print(Hotels)
        
        Webservice.web_map.webserviceFetchGetMethods(parameters: Hotels) { (parsedData, error, httpResponse) in
//            DispatchQueue.main.async {
                if(error){
                    let category = [CategoryData]()
                    completion?(category,false,NETWORKERROR)
                }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                    let info = parsedData["results"] as! [JSONType]
                    var category = [CategoryData]()
                    let hotelList = info.flatMap(CategoryData.init)
                    category.append(contentsOf: hotelList)
                    completion?(category,true,"Success")
                }else if httpResponse.statusCode == 401{
                    var Error : String!
                    Error = parsedData["message"] as! String
                    let category = [CategoryData]()
                    completion?(category,false,Error)
                }
//            }
        }
    }
}

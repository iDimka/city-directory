
import UIKit
import MapKit
import CoreData
import Foundation

import GoogleMobileAds


class CategoryDetailsVC: UIViewController  {
    @IBOutlet weak var tableReview: UITableView!
    @IBOutlet weak var collectionPhotos: UICollectionView!
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var viewBanner: UIView!
    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var viewPhotoCollection: UIView!
    @IBOutlet weak var viewReview: UIView!
    @IBOutlet weak var viewMap: UIView!
    
    @IBOutlet weak var btn_weakday: UIButton!
    @IBOutlet weak var favouriteButtonOutlet: UIButton!
    
    @IBOutlet weak var lblweak_day: UILabel!
    @IBOutlet weak var lblContactNo: UILabel!
    @IBOutlet weak var lblTimeSheet: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblLocationName: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var mapLocation: MKMapView!
    @IBOutlet weak var bluerviewOutlet: UIVisualEffectView!
    
    @IBOutlet weak var week_day_constraints: NSLayoutConstraint!
    @IBOutlet weak var week_day_height_constraints: NSLayoutConstraint!
    
    @IBOutlet weak var rating: RatingControl!
    
    var interstitialAd: GADInterstitial!
    
    var isLabelAtMaxHeight = true
    var upShow : Bool = true
    var viewcontext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    let user = UserDefaults.standard
    var APIList : [List] = [List]()
    var tempAPIList : [List]!
    var openNow = false
    var weak_day = [String]()
    var open_hours = [String:Any]()
    var images = [UIImage]()
    var placeID = String()
    var isPlace = Bool()
    var places = Place()
    var heights = 0
    var detailList = CategporyDetail()
    var locationData = CLLocationCoordinate2D(latitude: 0.0 ,longitude: 0.0)
    var number = ""
    var ratingnumber = 0.0
    
    // MARK: - LifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        self.week_day_constraints.constant = 10
        self.week_day_height_constraints.constant = 210
        self.lblweak_day.isHidden = true
        
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
        
        self.tableReview.rowHeight = UITableViewAutomaticDimension
        self.tableReview.estimatedRowHeight = 50
        self.tableReview.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        viewFirst.frame = (CGRect(x: 0, y: self.viewBanner.frame.size.height, width: self.view.frame.width, height: 480))
        self.scroll.addSubview(viewFirst)
        heights = 500
        
        let latitude = (String(appDelegate.latitude))
        let longitute11 = (String(appDelegate.longitude))
        let latitudeDegrees : CLLocationDegrees = Double(latitude)!
        let longitude : CLLocationDegrees = Double(longitute11)!
        
        self.locationData = CLLocationCoordinate2D(latitude: latitudeDegrees ,longitude: longitude)
        self.mapLocation.showsUserLocation = true
        let span = MKCoordinateSpan(latitudeDelta: 0.05,longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: self.locationData, span: span)
        self.mapLocation.setRegion(region, animated: true)
        
        rating.ratingCount = 1
        rating.ratingButton(button: 1)
        if isPlace{
            indicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
            indicator.center = view.center
            view.addSubview(indicator)
            indicator.bringSubview(toFront: view)
            indicator.hidesWhenStopped = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            indicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.setPlace()
            }
        }else{
            self.getCategoryDetail()
        }
    }
    
    func setPlace(){
        let uPath = itechBase.appending(places.photo[0].path!)
        let url = URL(string: uPath)
        let data = try? Data(contentsOf: url!)
        if data != nil {
            self.imgProfile.sd_setImage(with: url,placeholderImage: nil)
        }
        lblLocationName.text = places.name
        lblRating.text = places.rating
        self.rating.ratingCount = (Double(places.rating ?? String(0.0)))!
        self.rating.ratingButton(button: Double(places.rating ?? String(0.0))!)
        lblContactNo.text = places.contact
        lblTimeSheet.text = places.Timing
        let newAddress = (places.address!).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        lblAddress.text = newAddress//places.address
        btn_weakday.isEnabled = false
        
        DispatchQueue.main.async {
            if self.places.photo.count > 0{
                for i in 0..<self.places.photo.count{
                    let urlString = (itechBase).appending(self.places.photo[i].path!)
                    let url = URL(string: urlString)
                    let data = try? Data(contentsOf: url!)
                    if let imageData = data {
                        self.images.append(UIImage(data: imageData)!)
                    }
                }
            }
        }
        collectionPhotos.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.redesign()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.redesign()
        }
        indicator.stopAnimating()
        self.bluerviewOutlet.isHidden = true
    }
    
    // MARK: - IBAction methods (Click events)
    
    @IBAction func GetDirectionButton(_ sender: UIButton) {
        if isPlace{
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                //                UIApplication.shared.openURL(URL(string:
                //                    "comgooglemaps://?center=\(appDelegate.latitude),\(appDelegate.longitude)&zoom=14&views=traffic")!)
            }
        }else{
            let strLat : String = String(appDelegate.latitude)
            let strLong : String = String(appDelegate.longitude)
            
            let strLat1 : String = String(describing: self.detailList.location.lat!)
            let strLong2 : String = String(describing: self.detailList.location.lng!)
            
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(strLat),\(strLong)&daddr=\(strLat1),\(strLong2)&directionsmode=driving&zoom=14&views=traffic")!)
            }
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
//                UIApplication.shared.openURL(URL(string:
//                    "comgooglemaps://?center=\(appDelegate.latitude),\(appDelegate.longitude)&zoom=14&views=traffic")!)
            } else {
                print("Can't use comgooglemaps://")
                self.ShowAlertMessage(title: "", message: "No Google Map App In Your Device", buttonText: "OK")
            }
        }
    }
    
    @IBAction func CallToNumberButton(_ sender: UIButton) {
        var  newNumber = (number).replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
        newNumber = (newNumber).replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
        newNumber = (newNumber).replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
        if  NSURL(string: "TEL://" + newNumber) != nil {
            let url:NSURL = NSURL(string: "TEL://" + number)!
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    @IBAction func actionWeekDay(_ sender: UIButton) {
        if upShow {
            UIView.animate(withDuration: Double(0.5), animations: {
                self.week_day_constraints.constant = 10
                self.week_day_height_constraints.constant = 210
                self.lblweak_day.isHidden = true
                self.view.layoutIfNeeded()
                self.btn_weakday.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            })
        } else {
            UIView.animate(withDuration: Double(0.5), animations: {
                self.week_day_constraints.constant = 130
                self.week_day_height_constraints.constant = 320
                self.lblweak_day.isHidden = false
                self.view.layoutIfNeeded()
                self.btn_weakday.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            })
        }
        upShow = !upShow
        self.redesign()
    }
    
    // MARK: - Location manager delegate method (For get lat lang and set region)
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapLocation.setRegion(region, animated: true)
    }
    
    // MARK: - User define methods  (Set favourite using local data)
    
    func printData() {
        do {
            tempAPIList = try viewcontext.fetch(List.fetchRequest()) as! [List]
            for index in tempAPIList {
                if index.id == self.detailList.id {
                    favouriteButtonOutlet.isSelected = true
                    favouriteButtonOutlet.setImage(UIImage(named: "fill_heart"), for: .selected)
                    print("sapan" , tempAPIList)
                    break
                }   else  {
                    print("no image")
                }
            }
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
 
    // MARK: - User define methods (Get Category details) And set it
    
    func getCategoryDetail(){
        indicator.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubview(toFront: view)
        indicator.hidesWhenStopped = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        let datail = "&placeid=".appending(placeID)//("ChIJSdRbuoqEXjkRFmVPYRHdzk8")
            .appending("&key=").appending(Constants.GOOGLEKEY)
        Webservice.web_detail.webserviceFetchGetMethods(parameters: datail) { (parsedData, error, httpResponse) in
            DispatchQueue.main.async {
                if(error){
                    self.ShowAlertMessage(title: "", message: NETWORKERROR, buttonText: "OK")
                }else if httpResponse.statusCode >= 200 && httpResponse.statusCode <= 300{
                    if parsedData["result"] != nil{
                        let info = parsedData["result"] as! NSDictionary
                        self.detailList = CategporyDetail()
                        self.detailList.adr_address = info["adr_address"] as? String
                        self.detailList.formatted_address = info["formatted_address"] as? String
                        self.detailList.icon = info["icon"] as? String
                        self.detailList.id = info["id"] as? String
                        self.detailList.name = info["name"] as? String
                        if info["rating"] != nil {
                            self.ratingnumber = (info["rating"] as? Double!)!
                        }
                        self.printData()
                        self.lblLocationName.text = self.detailList.name
                        let latgeometry = info["geometry"] as! NSDictionary
                        let latLocation = latgeometry["location"] as! NSDictionary
                        self.detailList.location.lat = latLocation["lat"] as? Double
                        self.detailList.location.lng = latLocation["lng"] as? Double
                        if info["photos"] != nil {
                            let photo = info["photos"] as! [JSONType]
                            let pics = photo.flatMap(Photos.init)
                            self.detailList.photos.append(contentsOf: pics)
                        }
                        if info["rating"] != nil{
                            self.rating.ratingCount = (info["rating"] as? Double)!
                            self.rating.ratingButton(button: (info["rating"] as? Double)!)
                            self.lblRating.text = String(self.ratingnumber)
                        }else{
                            self.rating.ratingCount = 0
                            self.rating.ratingButton(button:0)
                            self.lblRating.text = "0"
                        }
                        if info["opening_hours"] != nil{
                            self.open_hours = info["opening_hours"] as! [String:Any]
                            self.openNow = self.open_hours["open_now"] as! Bool
                            self.weak_day = self.open_hours["weekday_text"] as! [String]
                            let day = Date().dayOfWeek()
                            self.lblweak_day.text = self.weak_day.joined(separator: "\n")
                            for index in 0..<self.weak_day.count {
                                let string = self.weak_day[index]
                                if string.range(of:day!) != nil {
                                    self.lblTimeSheet.text = string
                                }
                                if string.lowercased().range(of:day!) != nil {
                                    self.lblTimeSheet.text = string
                                } else {
                                    self.week_day_constraints.constant = 10
                                    self.week_day_height_constraints.constant = 210
                                    self.lblweak_day.isHidden = true
                                }
                            }
                        } else {
                            self.btn_weakday.isHidden = true
                            self.week_day_constraints.constant = 10
                            self.week_day_height_constraints.constant = 210
                            self.lblweak_day.isHidden = true
                        }
                        if info["reviews"] != nil{
                            let review = info["reviews"] as! [JSONType]
                            let reviewList = review.flatMap(Reviews.init)
                            self.detailList.reviews.append(contentsOf: reviewList)
                        }else{
                            print("Review not available")
                        }
                        if info["formatted_phone_number"] != nil {
                            self.lblContactNo.text = info["formatted_phone_number"] as? String
                            self.number = (info["formatted_phone_number"] as? String)!
                        }else{
                            print("Key not available")
                        }
                        self.lblAddress.text = info["formatted_address"] as? String
                        if self.detailList.photos.count > 0 {
                            let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=450").appending("&photoreference=").appending(self.detailList.photos[0].photo_reference!).appending("&key=").appending(Constants.GOOGLEKEY)
                            let url = URL(string: urlString)
                            let data = try? Data(contentsOf: url!)
                            if data != nil {
                                self.imgProfile.sd_setImage(with: url,placeholderImage: nil)
                            }
                        }
                        self.locationData = CLLocationCoordinate2D(latitude: self.detailList.location.lat! ,longitude: self.detailList.location.lng!)
                        self.mapLocation.showsUserLocation = true
                        let span = MKCoordinateSpan(latitudeDelta: 0.05,longitudeDelta: 0.05)
                        let region = MKCoordinateRegion(center: self.locationData, span: span)
                        self.mapLocation.setRegion(region, animated: true)
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = self.locationData
                        annotation.title = self.detailList.name//
                        self.mapLocation.addAnnotation(annotation)
                        self.redesign()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self.redesign()
                        }
                        for i in 0..<self.detailList.photos.count{
                            let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=\(self.detailList.photos[i].width!)").appending("&photoreference=").appending(self.detailList.photos[i].photo_reference!).appending("&key=").appending(Constants.GOOGLEKEY)
                            let url = URL(string:urlString)
                            if let data = try? Data(contentsOf: url!){
                                self.images.append((UIImage(data: data))!)
                            }
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.indicator.stopAnimating()
                        self.favouriteButtonOutlet.isHidden = false
                        self.bluerviewOutlet.isHidden = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                            self.setAd()
                        }
                    }
                }else{
                    let alertController = UIAlertController(title: "", message: "Some thing goes wrong please try again", preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func ShowAlertMessage(title : String, message: String, buttonText : String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Redesign UI using contant space
    func redesign(){
        let y = viewMap.frame.height + viewMap.frame.origin.y// + 10
        viewFirst.frame = (CGRect(x: 0, y: self.viewBanner.frame.size.height, width: self.view.frame.width, height: y))
        self.scroll.addSubview(viewFirst)
        heights = Int(y + self.viewBanner.frame.size.height)
        
        if images.count>0{
            viewPhotoCollection.frame = (CGRect(x: 0, y: heights, width: Int(self.view.frame.width), height: 190))
            self.scroll.addSubview(viewPhotoCollection)
            heights = heights + 170
        }
        self.tableReview.reloadData()
        var tableViewHeight: CGFloat {
            tableReview.layoutIfNeeded()
            return tableReview.contentSize.height
        }
        if self.detailList.reviews.count > 0{
            viewReview.frame = (CGRect(x: 0, y: heights, width: Int(self.view.frame.width), height: Int(tableViewHeight+74)))
            self.scroll.addSubview(viewReview)
            heights = heights + Int(tableViewHeight)+74
        }
        scroll.contentSize = CGSize(width:self.view.frame.width, height: CGFloat(heights))
    }
}

// MARK: - UICollactionview delegate and data source methods

extension CategoryDetailsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBAction func FavouriteButtonClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if favouriteButtonOutlet.isSelected {
            favouriteButtonOutlet.isSelected = true
            favouriteButtonOutlet.setImage(UIImage(named: "fill_heart"), for: .selected)
            let memberInfo : List = List(context: self.viewcontext)
            memberInfo.id = self.detailList.id
            memberInfo.name = self.detailList.name
            memberInfo.address = self.detailList.formatted_address
            memberInfo.rating = String(self.ratingnumber)
            memberInfo.placeid = self.placeID
            memberInfo.lat = self.detailList.location.lat!
            guard let imageData = UIImageJPEGRepresentation( imgProfile.image!, 1.0) else {
                return
            }//Rohit
            memberInfo.image = imageData
//            memberInfo.image = UIImage(imageData as NSData,scale:1.0)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
        else {
            favouriteButtonOutlet.isSelected = false
            favouriteButtonOutlet.setImage(UIImage(named: "empty_heart"), for: .normal)
            let memberInfo : List = List(context: self.viewcontext)
            memberInfo.id = self.detailList.id
            memberInfo.name = self.detailList.name
            memberInfo.address = self.detailList.formatted_address
            memberInfo.rating = String(self.ratingnumber)
            memberInfo.placeid = self.placeID
            memberInfo.lat = self.detailList.location.lat!
            memberInfo.long = self.detailList.location.lng!
            guard let imageData = UIImageJPEGRepresentation( imgProfile.image!, 1.0) else {
                return
            }
            memberInfo.image = imageData //as NSData
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            if let result = try? viewcontext.fetch(List.fetchRequest()) as! [List] {
                for object in result {
                    if object.id == self.detailList.id {
                        viewcontext.delete(object)
                        print("coredelete", object)
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }
                }
            }
        }
    }
    //1
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //2
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.images.count//self.detailList.photos.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                      for: indexPath) as! PhotosCollectionViewCell
         if self.images[indexPath.row] != nil {
           cell.photo_Img.image = self.images[indexPath.row]
        }
          return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewerController") as! ImageViewerController
        next.photo = self.images//[indexPath.row]
        next.pages = indexPath.row
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            return CGSize(width: (128), height: (90))
        
    }
}

// MARK: - UITableview delegate and data source methods

extension CategoryDetailsVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailList.reviews.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ReviewCell
        cell.section = indexPath
        cell.onButtonClick = {(indexPath) in
            if self.isLabelAtMaxHeight {
                cell.btnreadmore.setTitle("Read less", for: .normal)
                cell.lblDescription.numberOfLines = 0
            }
            else {
                cell.btnreadmore.setTitle("Read More", for: .normal)
                cell.lblDescription.numberOfLines = 4
            }
            self.isLabelAtMaxHeight = !self.isLabelAtMaxHeight
            self.tableReview.reloadRows(at: [indexPath], with: .none)
            self.redesign()
        }
        cell.lblName.text = self.detailList.reviews[indexPath.row].author_name
        cell.lblRating.text = String(self.detailList.reviews[indexPath.row].rating)
        cell.stackRating.ratingCount = self.detailList.reviews[indexPath.row].rating
        cell.stackRating.ratingButton(button: Double(self.detailList.reviews[indexPath.row].rating))
        cell.lblTimeAgo.text = ""
        cell.lblDescription.text = self.detailList.reviews[indexPath.row].text
        let profile_image = self.detailList.reviews[indexPath.row].profile_photo_url
        if profile_image != nil {
            let url = URL(string: profile_image!)
            DispatchQueue.main.async {
                cell.imgreview.sd_setImage(with: url,placeholderImage: nil)
            }
        }
        return cell
    }
}






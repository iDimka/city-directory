//
//  MoreOptionVC.swift
//  Near By
//
//  Created by itechnotion-mac1 on 06/04/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit
import OneSignal
import MessageUI

class MoreOptionVC: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var switchNotification: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedStringKey.foregroundColor:#colorLiteral(red: 0.1921568627, green: 0.2588235294, blue: 0.3490196078, alpha: 1)]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        let backItem = UIBarButtonItem(title: "Back", style: .bordered, target: nil, action: nil)
        navigationItem.backBarButtonItem = backItem
    }
    
    @IBAction func actionContactus(_ sender: Any) {
        self.sendEmail()
    }
    @IBAction func actionFAQ(_ sender: Any) {
    
    }
  
    @IBAction func actionShareApp(_ sender: Any) {
    
    }
    @IBAction func actionMapView(_ sender: Any) {
        let viewMap = self.storyboard?.instantiateViewController(withIdentifier: "NearbyMapVC") as! NearbyMapVC
        self.navigationController?.pushViewController(viewMap, animated: true)
    }
    
    @IBAction func actionNotioficationChange(_ sender: UISwitch) {
//        if sender.isOn {
//
//            sleep(3)
//            let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
//
//            // Replace '11111111-2222-3333-4444-0123456789ab' with your OneSignal App ID.
//            OneSignal.initWithLaunchOptions(launchOptions,
//                                            appId: "",
//                                            handleNotificationAction: nil,
//                                            settings: onesignalInitSettings)
//
//            OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
//
//
//        } else {
            print("Push Notification is OFF")
    //    }
    }
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["feedback@itechnotion.com"])
            mail.setSubject("City Directory iOS App")
            present(mail, animated: true)
        } else {
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

//
//  ViewController.swift
//  Near By
//
//  Created by itechnotion-mac1 on 26/03/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ratingControl: RatingControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        ratingControl.ratingCount = 3.5
        ratingControl.ratingButton(button: 4)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}



import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let itechBase = "http://itechnotion.in/"
//let mapBase = "https://maps.googleapis.com/maps/api/"
enum Webservice : String{
    case web_news = "cockpit/api/collections/get/news?token=940039fcdd2648a285e361d50070ca"
    case web_place = "cockpit/api/collections/get/place?token=940039fcdd2648a285e361d50070ca"
    
    case web_map =       "place/nearbysearch/json?"
    case web_detail = "place/details/json?"
    case web_loard_more = ""
    
    func webserviceFetchGetMethods( parameters: String, completion:(([String:Any],(Bool),(HTTPURLResponse))->())?) {
        let urlPath = Constants.WEBPATH.appending(self.rawValue.appending(parameters))
        
        let url = NSURL(string:urlPath as String)
       // print(url!)
        
        var request = URLRequest(url: url! as URL)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
         let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let httpResponse = response as? HTTPURLResponse
            if error != nil {
                DispatchQueue.main.async {
                    let parsedData:[String:Any] = [
                        "A":"A",
                        ]
                    completion?(parsedData,true,HTTPURLResponse())
                }
            }
            guard let data = data, error == nil else {
                print(error!)
                return
            }
            do {
                if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any] {
                    DispatchQueue.main.async {
                        completion?(parsedData, false,httpResponse!)
                    }
                }
            }
        }
        task.resume()
    }
    
    func webserviceFetchItech( parameters: String, completion:(([String:Any],(Bool),(HTTPURLResponse))->())?) {
        let urlPath = itechBase.appending(self.rawValue.appending(parameters))
        let url = NSURL(string:urlPath as String)
        var request = URLRequest(url: url! as URL)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let httpResponse = response as? HTTPURLResponse
            if error != nil {
                DispatchQueue.main.async {
                    let parsedData:[String:Any] = [  // ["b": 12]
                        "A":"A",
                        ]
                    completion?(parsedData,true,HTTPURLResponse())
                }
            }
            guard let data = data, error == nil else {
                print(error!)                                 // some fundamental network error
                return
            }
            do {
                if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any] {
                    DispatchQueue.main.async {
                        completion?(parsedData, false,httpResponse!)
                    }
                }
            }
        }
        task.resume()
    }

}

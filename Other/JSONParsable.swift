import Foundation

typealias JSONType = [String: Any]

protocol JSONParsable {
    init?(json: JSONType?)
}

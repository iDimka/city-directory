import Foundation

struct FAQList {
    var title: String!
    var description: [String]!
    var expanded: Bool!
    
    init(title: String, description: [String], expanded: Bool) {
        self.title = title
        self.description = description
        self.expanded = expanded
    }
}

//
//  StructureModal.swift
//  Near By
//
//  Created by itechnotion-mac1 on 28/03/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import Foundation
import UIKit


struct CategorySection {
    var id : String!
    var title = [Category]()
    
}


struct Category {
    var id : String!
    var title : String!
    var value : String!
}


struct CategoryList {
    var imgProfile : UIImage!
    var title : String!
    var id : String!
}


struct CategoryData {
    var icon : String?
    var id : String?
    var name : String?
    var photos = [Photos]()
    var place_id : String?
    var reference : String?
    var scope : String?
    var vicinity : String?
   
}

extension  CategoryData : JSONParsable {
    init?(json: JSONType?) {
        self.icon = json?["icon"] as? String
        self.id = json?["id"] as? String
        self.name = json?["name"] as? String
        if let info1 = json?["photos"]  as? [JSONType] {
            let photos = info1.flatMap(Photos.init)
            self.photos .append(contentsOf: photos)
        }
        
        self.place_id = json?["place_id"] as? String
        self.reference = json?["reference"] as? String
        self.scope = json?["scope"] as? String
        self.vicinity = json?["vicinity"] as? String
    }
}


struct Photos {
    var height : String?
    var photo_reference : String?
    var width : String!
    var images : UIImage?
}

extension Photos : JSONParsable {

    init?(json: JSONType?) {
        self.height = json?["height"] as? String
        self.width = String(json!["width"] as! Int)
//        print(self.width)
        
        self.photo_reference = json?["photo_reference"] as? String
//    
//        let urlString = Constants.PHOTOURL.appending("photo?").appending("maxwidth=170").appending("&photoreference=").appending( json?["photo_reference"] as! String).appending("&key=").appending(Constants.GOOGLEKEY)
//        let url = URL(string: urlString)
//        let data = try? Data(contentsOf: url!)
//      
//         
//        if let imageData = data {
//            //                cell.imgProfile.boderRoundwithShadow()
//            self.images = UIImage(data: imageData)
//        }
        
    }
}

struct Location {
    var lat : Double?
    var lng : Double?
}


struct CategporyDetail {
    
    var adr_address : String?
    var formatted_address : String?
    var location = Location()
        
    //Location()
    
    var icon : String?
    var id : String?
    var name : String?
    var photos = [Photos]()
    var place_id : String?
    var rating : String?
    var url : String?
    var vicinity : String?
    var formatted_phone_number : String?
    var opening_hours = OpeningHours()
    var reviews = [Reviews]()
    
}

struct Reviews {
    var author_name : String?
    var author_url : String?
    var language : String?
    var profile_photo_url : String?
    var rating : Double
    var relative_time_description : String?
    var text : String?
    var time : Double
}

extension Reviews : JSONParsable {
    init?(json: JSONType?) {
        self.author_name = json?["author_name"] as? String
        self.author_url = json?["author_url"] as? String
        self.language = json?["language"] as? String
        self.profile_photo_url = json?["profile_photo_url"] as? String
        self.rating = json?["rating"] as! Double
        self.relative_time_description = json?["relative_time_description"] as? String
        self.text = json?["text"] as? String
        self.time = json?["time"] as! Double
    }
}


struct OpeningHours{
    var open_now : String?
    var weekday_text : [String]?
    
}

struct SerachResultData {
    
    var location = Location()
    var icon : String?
    var id : String?
    var name : String?
    var place_id : String?
    var rating : Double?
    var vicinity : String?
    var photos = [Photos]()
    
}

extension SerachResultData  : JSONParsable {
    init?(json: JSONType?){
        let latgeometry = json?["geometry"] as! NSDictionary
        let latLocation = latgeometry["location"] as! NSDictionary
        self.location.lat = latLocation["lat"] as? Double
        self.location.lng = latLocation["lng"] as? Double
        
        self.icon = json?["icon"] as? String
        self.id = json?["id"] as? String
        self.name = json?["name"] as? String
        self.place_id = json?["place_id"] as? String
        self.rating = json?["rating"] as? Double
        self.vicinity = json?["vicinity"] as? String
        if let info1 = json?["photos"]  as? [JSONType] {
            let photos = info1.flatMap(Photos.init)
            self.photos .append(contentsOf: photos)
        }
    }
}

struct  NewsResult{
    var title : String?
    var category : String?
    var photo : JSONType?
        var path : String?
    var description : String?
    var date : String?
    var time : String?
    var _mby : String?
    var _by : String?
    var _modified : String?
    var _created : String?
    var _id : String?
    var author : String?
    var icon : UIImage?
}

extension NewsResult : JSONParsable{
    init?(json: JSONType?) {
        self.title = json?["title"] as? String
        self.category = json?["category"] as? String
        self.photo = json?["photo"] as? JSONType
        self.path = photo?["path"] as? String
        self.description = json?["description"] as? String
        self.date = json?["date"] as? String
        self.time = json?["time"] as? String
        self._mby = json?["_mby"] as? String
        self._by = json?["_by"] as? String
        self._modified = json?["_modified"] as? String
        self._created = json?["_created"] as? String
        self._id = json?["_id"] as? String
        self.author = json?["author"] as? String
        if let url = URL(string: path!){
            let data = try? Data(contentsOf: url)
            self.icon =  UIImage(data: data!)
        }
//        let url = URL(string: path!)
//        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//        self.icon =  UIImage(data: data!)
    }
    
   
}

struct Place {
    var Name : String?
    var Rating : String?
    var Contact : String?
    var Timing : String?
    var Category : String?
    var Address : String?
    var Location : String?
    var photo = [PlacePhoto]()
    var meta : String?
    var title : String?
    var asset : String?
    
    var _mby : String?
    var _by : String?
    var _modified : String?
    var _created : String?
    var _id : String?
    var name : String?
    var rating : String?
    var contact : String?
    var timing : String?
    var category : String?
    var address : String?
    var location : String?
}

extension Place : JSONParsable{
    init?(json: JSONType?) {
        self.Name = json?["Name"] as? String
        self.Rating = json?["Rating"] as? String
        self.Contact = json?["Contact"] as? String
        self.Timing = json?["Timing"] as? String
        self.Category = json?["Category"] as? String
        self.Address = json?["Address"] as? String
        self.Location = json?["Location"] as? String
        let pic = json?["photo"] as! [JSONType]
        let placePic = pic.flatMap(PlacePhoto.init)
        self.photo.append(contentsOf: placePic)
        self._mby = json?["_mby"] as? String
        self._by = json?["_by"] as? String
        self._modified = json?["_modified"] as? String
        self._created = json?["_created"] as? String
        self._id = json?["_id"] as? String
        self.name = json?["name"] as? String
        self.rating = json?["rating"] as? String
        self.contact = json?["contact"] as? String
        self.timing = json?["timing"] as? String
        self.category = json?["category"] as? String
        self.address = json?["address"] as? String
        self.location = json?["location"] as? String
    }
}

struct PlacePhoto {
    var path : String?
}

extension PlacePhoto : JSONParsable{
    init?(json: JSONType?) {
        self.path = json?["path"] as? String
    }
}

//

import Foundation
import UIKit

struct Constants {
    static let WEBPATH = "https://maps.googleapis.com/maps/api/"
    static let GOOGLEKEY = "AIzaSyAF4dWi2Ax4T1Onec8DktvU8lABJnlZ2TM" //Live
    //"AIzaSyBtMkD9llo1keDCV0fDt4v4TaZpFrH57gI" //Test
    static let PHOTOURL = "https://maps.googleapis.com/maps/api/place/"
    static let RADIUS = "50000"//radius
    static let SENSOR = "true"//sensor
    
    static let ADUNIT = "ca-app-pub-7770856719889795~4943715437"
    static let ADBANNER = "ca-app-pub-7770856719889795/2659073699"//sensor
    static let ADINTERSTITIAL = "ca-app-pub-7770856719889795/7625719091"//sensor
    
    static let ONESGNALAPPID = "5cbdf8ac-68c9-42ae-bd66-55066af45b15"
}


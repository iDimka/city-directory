//
//  AdMobVC.swift
//  Near By
//
//  Created by itechnotion on 10/07/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds

extension CategoryDetailsVC : GADInterstitialDelegate{
//Full screen GoogleAd
    func setAd(){
        interstitialAd = GADInterstitial(adUnitID: Constants.ADINTERSTITIAL)
        let request = GADRequest()
        interstitialAd.load(request)
        interstitialAd.present(fromRootViewController: self)
        interstitialAd.delegate = self
        if interstitialAd.isReady {
            interstitialAd.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }

    // Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        interstitialAd.present(fromRootViewController: self)
        print("interstitialDidReceiveAd")
    }
    // Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    // Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    // Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    // Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
    }
    
    // Tells the delegate that a user click will open another app
    // (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}

extension HomeVC : GADBannerViewDelegate{
    //set for banner ad
    func setAd(){
        // In this case, we instantiate the banner with desired ad size.
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        bannerView.transform = CGAffineTransform.identity
        self.addBannerViewToView(bannerView)
        bannerView.adUnitID = Constants.ADBANNER
        
        bannerView.rootViewController = self
        bannerView.transform = CGAffineTransform.identity
        //Fot testing device (Before upload it on app store)
            let request: GADRequest = GADRequest()
            request.testDevices = ["2de9a515deb604931465fcb6804845f1", kGADSimulatorID]
            bannerView.load(request)
        //
        bannerView.delegate = self
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = true
        //        let adSize = GADAdSizeFromCGSize(CGSize(width: 300, height: 50))
        //        viewAddBanner = self.bannerView
        viewAddBanner.addSubview(bannerView)
        viewAddBanner.frame = CGRect(x: viewAddBanner.frame.origin.x, y: viewAddBanner.frame.origin.y, width: viewAddBanner.frame.size.width, height: viewAddBanner.frame.size.height)
        //        viewNews.addSubview(bannerView)
        //        viewNews.addConstraints(
        //            [NSLayoutConstraint(item: bannerView,
        //                                attribute: .bottom,
        //                                relatedBy: .equal,
        //                                toItem: bottomLayoutGuide,
        //                                attribute: .top,
        //                                multiplier: 1,
        //                                constant: 0),
        //             NSLayoutConstraint(item: bannerView,
        //                                attribute: .centerX,
        //                                relatedBy: .equal,
        //                                toItem: view,
        //                                attribute: .centerX,
        //                                multiplier: 1,
        //                                constant: 0)
        //            ])
    }
    // Tells the delegate an ad request loaded an ad.
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        // Add banner to view and add constraints as above.
               addBannerViewToView(bannerView)
    }
    
    // Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    // Tells the delegate that a full-screen view will be presented in response
    // to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    // Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    // Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    // Tells the delegate that a user click will open another app (such as
    // the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
}

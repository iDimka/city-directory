//
//  CategorySearchCell.swift
//  Near By
//
//  Created by itechnotion-mac1 on 26/03/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class CategorySearchCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var stackRate: RatingControl!
    @IBOutlet weak var lblDistance: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

//
//  NewsHeader.swift
//  Near By
//
//  Created by itechnotion on 29/06/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class NewsHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

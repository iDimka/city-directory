//
//  ReviewCell.swift
//  Near By
//
//  Created by itechnotion-mac1 on 03/04/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

   // var delegate: CellSubclassDelegate?
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAbbrivation: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblTimeAgo: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgreview: UIImageView!
    @IBOutlet weak var btnreadmore: UIButton!
    @IBOutlet weak var stackRating: RatingControl!
    var onButtonClick: ((IndexPath) -> ())?
    var section: IndexPath?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func ReadMoreButton(_ sender: UIButton) {
        
        
        if let section = self.section {
        onButtonClick?(section)
        }
    
    
    
}
    
}

//protocol CellSubclassDelegate: class {
//   func buttonTapped(cell: ReviewCell)
//}




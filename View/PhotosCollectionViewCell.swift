//
//  PhotosCollectionViewCell.swift
//  Near By
//
//  Created by itechnotion on 18/05/18.
//  Copyright © 2018 itechnotion-mac1. All rights reserved.
//

import UIKit

class PhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var photo_Img: UIImageView!
    @IBOutlet weak var view2: UIView!
}
